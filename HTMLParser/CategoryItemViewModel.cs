﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTMLParser
{
    public class CategoryItemViewModel
    {
        public string uID { get; set; }
        public string title { get; set; }
        public string province { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string tel { get; set; }
        public string path { get; set; }
        public string content { get; set; }

        public string address { get; set; }
        public string category { get; set; }

        public string website { get; set; }
        public string email { get; set; }

        //public string anagraficheTrovate { get; set; }
        //public string lat { get; set; }
        //public string lng { get; set; }
    }
}
