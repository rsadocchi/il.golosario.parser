﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTMLParser
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start main grabbing...");
            var htmlService = new HtmlService();
            htmlService.LoadCategories().Wait();
            htmlService.LoadCategoryResults().Wait();

            Console.WriteLine();
            Console.WriteLine("Start Single Page grabbing...");
            htmlService.Results
                .ToList()
                .ForEach(k =>
                {
                    for (int idx = 0; idx < k.Value.Count(); idx++)
                        Console.WriteLine(htmlService.SinglePageGrabberAsync(k.Value[idx].path,k.Key,idx).Result);
                });

            Console.WriteLine();
            Console.WriteLine("Writing file...");
            string file = System.IO.Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                "IlGolosario.xlsx");

            var excelService = new ExcelService(file, htmlService.Results);
            excelService.GenerateFileXls().Wait();

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Grab end");
            Console.ReadLine();
        }
    }
}
