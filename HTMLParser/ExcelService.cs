﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;

namespace HTMLParser
{
    public class ExcelService
    {

        public Dictionary<string, List<CategoryItemViewModel>> Source { get; set; }
        public string FilePath { get; set; }

        public ExcelService() { }

        public ExcelService(string filePath, Dictionary<string, List<CategoryItemViewModel>> source)
        {
            FilePath = filePath;
            Source = source;
        }

        public async Task<bool> GenerateFileXls()
        {
            if (string.IsNullOrWhiteSpace(FilePath)) return false;
            if (Source == null || Source.Count <= 0) return false;

            if (!Directory.Exists(Path.GetDirectoryName(FilePath)))
                Directory.CreateDirectory(Path.GetDirectoryName(FilePath));

            //using (var f = File.Create(FilePath)) { }
            var file = new FileInfo(FilePath);
            using (var pkg = new ExcelPackage(file))
            {
                bool intestazioni = false;
                Source.ToList().ForEach(kv =>
                {
                    var data = kv.Value;
                    var ws = pkg.Workbook.Worksheets.Add(kv.Key);

                    //intestazioni colonne
                    if (!intestazioni)
                    {
                        ws.Cells[1, 1].Value = "Ragione Sociale";
                        ws.Cells[1, 2].Value = "Provincia";
                        ws.Cells[1, 3].Value = "Città";
                        ws.Cells[1, 4].Value = "Indirizzo";
                        ws.Cells[1, 5].Value = "Telefono";
                        ws.Cells[1, 6].Value = "Email";
                        ws.Cells[1, 7].Value = "Website";
                        ws.Cells[1, 8].Value = "Link";
                        ws.Cells[1, 9].Value = "Descrizione";
                        intestazioni = true;
                    }

                    // dati
                    int rowIndex = 2;
                    data.ForEach(row => // https://www.ilgolosario.it
                    {
                        ws.Cells[rowIndex, 1].Value = $"{row.title}";
                        ws.Cells[rowIndex, 2].Value = $"{row.province}";
                        ws.Cells[rowIndex, 3].Value = $"{row.city}";
                        ws.Cells[rowIndex, 4].Value = $"{row.street}";
                        ws.Cells[rowIndex, 5].Value = $"{row.tel}";
                        ws.Cells[rowIndex, 6].Value = $"{row.email}";
                        ws.Cells[rowIndex, 7].Value = $"{row.website}";
                        ws.Cells[rowIndex, 8].Value = $"https://www.ilgolosario.it/{row.path}";
                        ws.Cells[rowIndex, 9].Value = $"{row.content}";
                        rowIndex += 1;
                    });

                    intestazioni = false;
                });

                pkg.Save();
            }
            return await Task.FromResult(true);
        }

    }
}
