﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Newtonsoft.Json;

namespace HTMLParser
{
    public class HtmlService
    {
        private string _baseUrl { get { return "https://www.ilgolosario.it"; } }
        private readonly HttpClient client = new HttpClient();
        private readonly HtmlDocument document = new HtmlDocument();

        public Dictionary<string, List<CategoryItemViewModel>> Results;

        public HtmlService()
        {
            Results = new Dictionary<string, List<CategoryItemViewModel>>();
        }

        public async Task<bool> LoadCategories()
        {
            var httpRespMessage = await client.GetAsync(_baseUrl);
            using (var httpStream = await httpRespMessage.Content.ReadAsStreamAsync())
            {
                document.Load(httpStream);
                HtmlNodeCollection nodes = document.DocumentNode.SelectNodes("//select");
                foreach (var node in nodes)
                    if (node.Id == "hCategory")
                        (node.ChildNodes.Select(c => c.GetAttributeValue("value", "")).ToList())
                            .ForEach(x => { if (!string.IsNullOrEmpty(x)) Results.Add(x, null); });
                return await Task.FromResult(true);
            }
        }

        public async Task<bool> LoadCategoryResults()
        {
            foreach (var cat in Results.Keys.ToArray())
            {
                try
                {

                    int risultati = 0;
                    int numeroSlide = 0;

                    var httpRespMessage = await client.GetAsync($"{_baseUrl}/search/{cat}");
                    using (var httpStream = await httpRespMessage.Content.ReadAsStreamAsync())
                    {
                        document.Load(httpStream);
                        var risultatiString = document.DocumentNode.SelectNodes("//span")
                            .Where(d => d.GetAttributeValue("class", "") == "numeroRisultati")
                            .FirstOrDefault();
                        int.TryParse(risultatiString.InnerText, out risultati);
                    }


                    while (numeroSlide < risultati)
                    {
                        try
                        {
                            var values1 = new Dictionary<string, string>() { { "ajax_request", "1" }, { "next_from", $"{numeroSlide}" } };
                            var content1 = new FormUrlEncodedContent(values1);
                            var response1 = await client.PostAsync($"{_baseUrl}/search/{cat}", content1);
                            var responseJson = await response1.Content.ReadAsStringAsync();
                            var responseObj = new List<CategoryItemViewModel>();
                            try
                            {
                                if (string.IsNullOrEmpty(responseJson) || responseJson.Length <= 10) throw new Exception("Json vuoto");
                                responseObj = JsonConvert.DeserializeObject<CategoryItemViewModel[]>(responseJson).ToList();
                            }
                            catch
                            {
                                continue;
                            }

                            var values2 = new Dictionary<string, string>() { { "ajax_request", "2" }, { "next_from", $"{numeroSlide}" } };
                            var content2 = new FormUrlEncodedContent(values2);
                            var response2 = await client.PostAsync($"{_baseUrl}/search/{cat}", content2);
                            var responseHtml = await response2.Content.ReadAsStringAsync();

                            document.LoadHtml(responseHtml);
                            var divSupplier = document.DocumentNode.SelectNodes("//div")
                                .Where(d => d.GetAttributeValue("data-rel", "").StartsWith($"/{cat}/"))
                                .ToList();

                            foreach (var spl in divSupplier)
                            {
                                try
                                {
                                    var path = spl.GetAttributeValue("data-rel", "");
                                    var match = Regex.Match(spl.InnerText.Trim(), @"(tel:\s\d+)", RegexOptions.IgnoreCase);
                                    if (match.Value.Length > 5)
                                        responseObj.Where(o => o.path == path).FirstOrDefault().tel = match.Value.Substring(5);
                                    else
                                        responseObj.Where(o => o.path == path).FirstOrDefault().tel = "ND";
                                }
                                catch
                                {
                                    //throw ex;
                                    continue;
                                }
                            }

                            if (Results[cat] == null) Results[cat] = new List<CategoryItemViewModel>();
                            foreach (var item in responseObj)
                                Results[cat].Add(item);

                            numeroSlide += responseObj.Count();
                        }
                        catch 
                        {
                            //throw ex;
                            continue;
                        }
                    }
                }
                catch 
                {
                    //throw ex;
                    continue;
                }
            }

            Results = Results.Where(o => o.Value != null && o.Value.Count > 0).ToDictionary(k => k.Key, v => v.Value);
            return await Task.FromResult(true);
        }

        public async Task<string> SinglePageGrabberAsync(string url, string category, int idx)
        {
            if (string.IsNullOrWhiteSpace(url)) return $"empty - {url}";
            try
            {
                HtmlNode divAnagrafica = null;
                var httpRespMessage = await client.GetAsync($"{_baseUrl}{url}");
                using (var httpStream = await httpRespMessage.Content.ReadAsStreamAsync())
                {
                    document.Load(httpStream);
                    divAnagrafica = document.DocumentNode.SelectNodes("//div")
                        .Where(d => d.GetAttributeValue("class", "").Contains("anagrafica"))
                        .FirstOrDefault();
                }

                if (divAnagrafica == null) return $"empty - {url}";

                List<HtmlNode> links = new List<HtmlNode>();

                divAnagrafica.ChildNodes
                    .Where(n => n.Name == "p")
                    .Select(n => n)
                    .ToList()
                    .ForEach(n =>
                    {
                        var anchor = n.ChildNodes
                            .Where(a => a.Name == "a")
                            .Select(a => a)
                            .FirstOrDefault();

                        if (anchor != null) links.Add(anchor);
                    });

                if (links.Count() <= 0) return $"empty - {url}";

                string web = "";
                string email = "";

                links.ForEach(a =>
                {
                    var _email = a.Attributes
                        .Where(h => h.Value.ToLower().StartsWith("mailto:"))
                        .Select(h => h.Value.Replace("mailto:", ""))
                        .FirstOrDefault();
                    if (!string.IsNullOrWhiteSpace(_email)) email = _email;

                    var _web = a.Attributes
                        .Where(h => h.Value.ToLower().StartsWith("https://") || h.Value.ToLower().StartsWith("http://"))
                        .Select(h => h.Value)
                        .FirstOrDefault();
                    if (!string.IsNullOrWhiteSpace(_web)) web = _web;
                });

                var item = Results[category][idx];
                item.website = web;
                item.email = email;

                int match = 0;
                if (!string.IsNullOrWhiteSpace(email)) match += 1;
                if (!string.IsNullOrWhiteSpace(web)) match += 1;
                return $"{match} match - {url}";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
