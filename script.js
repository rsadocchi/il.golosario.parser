$('#bx-next').click(function() {
        var numeroSlide = $('.contenitoreSlideAnag ul li').length;
        numeroSlide = numeroSlide * 2;
        var contaSlide = sliderCardsAnagrafica.getCurrentSlide();
        if ($('#bx-next a').hasClass('disabled')) {
            $.ajax({
                type: "POST",
                dataType: "html",
                url: location.href,
                context: this,
                data: {
                    ajax_request: '2',
                    next_from: numeroSlide
                },
                success: function(j) {
                    var content = '';
                    content += j;
                    $('.bxsliderAnagrafica').append(content);
                    sliderCardsAnagrafica.reloadSlider({
                        startSlide: contaSlide,
                        pager: false,
                        controls: true,
                        infiniteLoop: false,
                        hideControlOnEnd: true,
                        minSlides: 1,
                        maxSlides: 5,
                        slideWidth: 225,
                        nextSelector: '#bx-next',
                        prevSelector: '#bx-prev',
                        onSliderLoad: function() {
                            $(".aggregatore .bx-wrapper").css("max-width", "100%");
                        }
                    });
                    $('#bx-next a').removeClass('disabled');
                },
                error: function(j) {
                    console.log('errore');
                }
            });
        }
    });